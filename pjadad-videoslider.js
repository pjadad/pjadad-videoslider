var videoSlider = (function($) {
	var isMobile = ("ontouchstart" in window || navigator.maxTouchPoints);
	var debugMode = false;

	function log() {
		if (debugMode) {
			console.log(Array.prototype.slice.call(arguments).join(', '));
		}
	}

	function setCaption($el) {
		var $caption = $('<span />').text($el.data('caption'));
		var link = $el.data('link');
		
		$('.videoslider__caption').html(
			link ? $('<a />')
				.attr('href', link)
				.html($caption) :
				$caption
		);
	}

	var desktop = {		
		init: function() {
			log('init');

			// reveal desktop slider
			$('.videoslider__desktop').addClass('active');

			// debounced resize event
			$(window).on('resize', _.debounce(this.windowResizeHandler.bind(this), 20));

			// Tab inactive state handler
			$(document).on('visibilitychange', this.visibilitychangeHandler.bind(this));

			// add loading screens
			this.buildLoadingScreens();

			// initialize swiper
			this.swiper = new Swiper($('.videoslider__desktop .swiper-container'), {
				effect: 'fade',
	      speed: 400,
	      loop: true,
	      keyboardControl: true
	    });

			// attach swiper events
	   	this.swiper.on('transitionStart', this.transitionStartHandler.bind(this));
    	this.swiper.on('transitionEnd', this.transitionEndHandler.bind(this));

    	// trigger
    	this.transitionEndHandler(); // needs to be triggerde before start, to make sure video exist
			this.transitionStartHandler(); // ... plays video
		},
		
		// resumes current video if it was paused by the browser when the tab was inactive
		visibilitychangeHandler: function(e) {
    if (document.visibilityState === 'visible') {
      var $activeSlide = this.getActiveSlide();
      var $activeSlideVideo = $activeSlide.find('video');

      if ($activeSlideVideo.length > 0 && $activeSlideVideo.get(0).paused) {
      	$activeSlideVideo.get(0).play();
      }
    }
		},

		transitionEndHandler: function() {
			log('swiper:end');

			this.setupRelevantSlides();

			// pause and reset all possible videos
			// except the active one
			$('.videoslider__desktop .swiper-slide')
				.not(this.getActiveSlide())
				.each(function() {
					var $video = $(this).find('video');

					if ($video.length > 0) {
						if (!$video.get(0).paused) {
							log('video:pause');
		        	$video.get(0).pause();
	  	      	$video.get(0).currentTime = 0;
	  	    	}
					}
			});
		},

		transitionStartHandler: function() {
			log('swiper:start');

			var $slide = this.getActiveSlide();

			this.playSlideVideo($slide);
			setCaption(this.getActiveSlideItem());
		},

		setupSlideItem: function($slide) {
			log('setupSlideItem');

			var $item = $slide.find('.videoslider__desktop-item');

			this.buildVideo($item);
			this.setImage($item);
		},

		setupRelevantSlides: function() {
			var realSlideCount = this.swiper.slides.length - 2;
			var realIndex = this.swiper.realIndex;
			var relevantIndexArr = [];
			var self = this;

			// collect relevant indexes
			relevantIndexArr.push(realIndex === realSlideCount ? 0 : realIndex + 1); // prev index
			relevantIndexArr.push(realIndex === 0 ? realSlideCount - 1 : realIndex - 1); // next index
			relevantIndexArr.push(realIndex); // active index

			// this will find next, prev and active + it's potential duplicates
			$('.videoslider__desktop .swiper-slide').each(function() {
				var $slide = $(this);
				var index = $slide.data('swiper-slide-index');

				if (relevantIndexArr.indexOf(index) !== -1) {
					self.setupSlideItem($slide);
				}	
			});
		},

		buildVideo: function($item) {
			log('buildVideo');

			// exit if already loaded
			if ($item.hasClass('video-loaded')) return;			

			var videoSrcArr = $item
				.data('video-src')
				.split(', ');

			var	$video = $('<video />');

			// append all possible video sources to the video
			_.each(videoSrcArr, function(src) {
				var	ext = src.split('.').pop();

				$video
					.append($('<source />')
						.attr('src', src)
						.attr('type', 'video/' + ext));
			});

			// set attributes, attach events & load
			$video
				.prop('muted', true)
				.on('ended', this.swiper.slideNext)
				.on('loadedmetadata', this.resizeVideo)
				.on('canplaythrough', this.videoCanPlayHandler)
				.on('pause', function() {
					console.log('video paused');
				})
				.load();

			// append video
			$item
				.append($video)
				.addClass('video-loaded');
		},

		playSlideVideo: function($slide) {
			var $video = $slide.find('video');

			if ($video.length > 0) {
				$video.get(0).play();
			}
		},

		setImage: function($item) {
			log('setImage');

			// exit if already loaded
			if ($item.hasClass('image-loading') || $item.hasClass('image-loaded')) return;

			var imgSrc = $item.data('image-src');

			$('<img />')
				.on('load', this.imageLoadHandler.bind(this, $item.get(0)))
				.attr('src', imgSrc);
		},

		imageLoadHandler: function(item, event) {
			var imgSrc = $(event.target).attr('src');

			// add background image to item
			$(item)
				.css('background-image', 'url(' + imgSrc + ')')
				.removeClass('image-loading')
				.addClass('image-loaded');
		},

		videoCanPlayHandler: function(event) {
			log('video:canplay');

			$(event.target)
				.closest('.videoslider__desktop-item')
				.addClass('video-canplay');
		},

		getActiveSlide: function() {
			return $('.videoslider__desktop .swiper-slide-active');
		},

		getActiveSlideItem: function() {
			return this.getActiveSlide().find('.videoslider__desktop-item');
		},

		windowResizeHandler: function() {
			$('.videoslider__desktop .swiper-slide video').each(this.resizeVideo);
		},

		resizeVideo: function() {
			log('resizeVideo');

			var video = this;
			var $video = $(this);
			var $container = $('.videoslider__desktop');
			var fillPixels = 2; // prevents whitespace

			var containerWidth = $container.width();
			var containerHeight = $container.height();

    	if (containerWidth / video.videoWidth > containerHeight / video.videoHeight) {
	      $video.css({
	        width: containerWidth + fillPixels,
	        height: 'auto'
	      });
	    } else {
	      $video.css({
	        width: 'auto',
	        height: containerHeight + fillPixels
	      });
	    }
		},

		buildLoadingScreens: function() {
			$('.videoslider__desktop-item').each(function() {
				$(this).append('<div class="videoslider__desktop-loading" />');
			});
		}

	};

	var mobile = {

		init: function() {
			log('mobile:init');

			// reveal mobile slider
			$('.videoslider__mobile').addClass('active');

			// add loading screens
			this.buildLoadingScreens();

			this.swiper = new Swiper($('.videoslider__mobile .swiper-container'), {
	      speed: 400,
	      loop: true,
	      autoplay: 6000
	    });

			// attach swiper events
    	this.swiper.on('transitionEnd', this.transitionEndHandler.bind(this));

    	// trigger
    	this.transitionEndHandler();
		},

		transitionEndHandler: function() {
			log('mobile:swiper:end');

			this.setupRelevantSlides();
			setCaption(this.getActiveSlide());
		},
		
		setupRelevantSlides: function() {
			var realSlideCount = this.swiper.slides.length - 2;
			var realIndex = this.swiper.realIndex;
			var relevantIndexArr = [];
			var self = this;

			// collect relevant indexes
			relevantIndexArr.push(realIndex === realSlideCount ? 0 : realIndex + 1); // prev index
			relevantIndexArr.push(realIndex === 0 ? realSlideCount - 1 : realIndex - 1); // next index
			relevantIndexArr.push(realIndex); // active index

			// this will find next, prev and active + it's potential duplicates
			$('.videoslider__mobile .swiper-slide').each(function() {
				var $slide = $(this);
				var index = $slide.data('swiper-slide-index');

				if (relevantIndexArr.indexOf(index) !== -1) {
					self.setImage($slide);
				}	
			});
		},

		setImage: function($slide) {
			log('mobile:setImage');

			// exit if already loaded
			if ($slide.hasClass('image-loading') || $slide.hasClass('image-loaded')) return;

			var imgSrc = $slide.data('image-src');

			$('<img />')
				.on('load', this.imageLoadHandler.bind(this, $slide.get(0)))
				.attr('src', imgSrc);
		},

		imageLoadHandler: function(item, event) {
			var imgSrc = $(event.target).attr('src');

			// add background image to slide
			$(item)
				.css('background-image', 'url(' + imgSrc + ')')
				.removeClass('image-loading')
				.addClass('image-loaded');
		},

		getActiveSlide: function() {
			return $('.videoslider__mobile .swiper-slide-active');
		},

		buildLoadingScreens: function() {
			log('mobile:buildLoadingScreens');

			$('.videoslider__mobile .swiper-slide').each(function() {
				$(this).append('<div class="videoslider__mobile-loading" />');
			});
		}

	};

	// expose init
	return function() {
		if ($('.videoslider').length > 0) {
			(isMobile ?
				mobile : 
				desktop).init();
		}
	};
})(jQuery);